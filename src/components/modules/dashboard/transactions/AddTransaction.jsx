import { Button, NumberInput, Select, TextInput } from "@mantine/core";
import React, { useEffect, useState } from "react";
import { GetRequest, PostRequest } from "../../../../plugins/https";

const AddTransaction = (props) => {
  const { close, parentTransactions, setParentTransactions } = props;
  const [category, setCategory] = useState([]);
  const [account, setAccount] = useState([]);

  useEffect(() => {
    const getData = async () => {
      const res = await GetRequest("/category");
      console.log(res.data);
      let modifiedCategory = [];
      res.data.map((v, key) => {
        modifiedCategory.push({
          label: v.title,
          value: v._id,
        });
      });
      setCategory(modifiedCategory);

      const accountRes = await GetRequest("/account");
      console.log(accountRes.data);
      let modifiedAccount = [];
      accountRes.data.map((v, key) => {
        modifiedAccount.push({
          label: v.title,
          value: v._id,
        });
      });

      setAccount(modifiedAccount);
    };

    getData();
  }, []);

  const [transaction, setTransaction] = useState({
    title: "",
    amount: "",
    category: "",
    account: "",
  });

  const handleInput = (e) => {
    setTransaction({
      ...transaction,
      [e.target.name]: e.target.value,
    });
  };

  const submitForm = async (e) => {
    e.preventDefault();
    console.log(transaction);

    const res = await PostRequest("/transaction", transaction);
    console.log(res.data);

    setParentTransactions([...parentTransactions, res.data]);

    close();
  };

  return (
    <div>
      {" "}
      <form onSubmit={submitForm} className="flex flex-col gap-sm">
        <div>
          <div>Title</div>
          <TextInput
            name="title"
            onChange={handleInput}
            value={transaction.title}
          />
        </div>
        <div>
          <div>Amount</div>
          <TextInput
            name="amount"
            onChange={handleInput}
            value={transaction.amount}
          />
        </div>
        <div>
          <div>Category</div>
          <Select
            onChange={(e) => {
              setTransaction({
                ...transaction,
                category: e,
              });
            }}
            value={transaction.category}
            data={category}
          />
        </div>{" "}
        <div>
          <div>Account</div>
          <Select
            onChange={(e) => {
              setTransaction({
                ...transaction,
                account: e,
              });
            }}
            value={transaction.account}
            data={account}
          />
        </div>
        <Button color="orange" className="mt-sm" type="submit">
          Submit
        </Button>
      </form>
    </div>
  );
};

export default AddTransaction;
