import React from "react";
import DashboardTransactions from "../../components/modules/dashboard/transactions/DashboardTransactions";

const Transactions = () => {
  return (
    <div>
      <DashboardTransactions />
    </div>
  );
};

export default Transactions;
